/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kiroussa <oss@xtrm.me>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/21 17:11:42 by kiroussa          #+#    #+#             */
/*   Updated: 2024/10/13 20:14:58 by kiroussa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <tocard/engine.h>
#include <tocard/window.h>

#include <stdio.h>

int	main(void)
{
	t_toc_engine	*engine;
	t_toc_window	*window;

	engine = toc_engine_create();
	if (!engine)
		return (1);
	window = toc_window_create(engine, "Empty Window", 800, 600);
	if (!window)
	{
		printf("Failed to create window\n");
		toc_engine_destroy(engine);
		return (1);
	}
	toc_engine_await(engine);
	toc_engine_destroy(engine);
	return (0);
}
