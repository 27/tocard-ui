/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kiroussa <oss@xtrm.me>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/21 17:11:42 by kiroussa          #+#    #+#             */
/*   Updated: 2024/04/21 20:10:49 by kiroussa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <tocard/engine.h>
#include <tocard/window.h>
#include <stdio.h>

int	main(void)
{
	t_toc_engine	*engine;
	t_toc_window	*window;
	t_toc_window	*window2;

	engine = toc_engine_create();
	if (!engine)
		return (1);
	window = toc_window_create(engine, "Empty Window", 800, 600);
	if (!window)
		toc_engine_destroy(engine);
	if (!window)
		return (1);
	window2 = toc_window_create(engine, "Empty Window 2", 800, 600);
	if (!window2)
	{
		toc_window_close(window);
		toc_engine_destroy(engine);
		return (1);
	}
	printf("Awaiting engine\n");
	toc_engine_await(engine);
	printf("No window left, creating a new one\n");
	window = toc_window_create(engine, "Empty Window 3", 800, 600);
	if (window)
	{
		printf("Awaiting engine\n");
		toc_engine_await(engine);
	}
	printf("Destroying engine\n");
	toc_engine_destroy(engine);
	return (0);
}
