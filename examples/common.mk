# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    common.mk                                          :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: kiroussa <oss@xtrm.me>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2024/04/20 18:33:47 by kiroussa          #+#    #+#              #
#    Updated: 2024/10/13 20:07:11 by kiroussa         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = $(notdir $(shell pwd)).out

CWD = $(shell pwd)

LIBTOCARD_DIR = ../..
LIBTOCARD = $(CWD)/$(LIBTOCARD_DIR)/build/libtocard.so

LIBMLX_DIR = $(LIBTOCARD_DIR)/MacroLibX
LIBMLX = $(CWD)/$(LIBMLX_DIR)/libmlx.so

SRC = $(wildcard *.c) ../sighandler.c
OBJ = $(SRC:.c=.o)

CC = clang
CFLAGS = -Wall -Wextra -Werror -gdwarf-4 -g3
LDFLAGS = $(LIBMLX) $(LIBTOCARD) -lSDL2 -rdynamic
INCLUDES = -I$(LIBTOCARD_DIR)/include -I$(LIBMLX_DIR)/includes

all: maketocard $(NAME)

$(NAME): $(LIBMLX) $(LIBTOCARD) $(OBJ)
	$(CC) $(CFLAGS) $(INCLUDES) -o $@ $(OBJ) $(LDFLAGS)

$(LIBMLX_DIR):
	@git clone https://github.com/seekrs/MacroLibX.git --recursive $(LIBMLX_DIR)

$(LIBMLX): $(LIBMLX_DIR)
	$(MAKE) -C $(LIBMLX_DIR) -j$(shell nproc)

maketocard:
	$(MAKE) -C $(LIBTOCARD_DIR) -j$(shell nproc) DEBUG=1

%.o: %.c
	$(CC) $(CFLAGS) $(INCLUDES) -c -o $@ $<

clean:
	rm -f $(OBJ)
	$(MAKE) -C $(LIBMLX_DIR) clean
	$(MAKE) -C $(LIBTOCARD_DIR) clean

fclean: clean
	rm -f $(NAME)
	$(MAKE) -C $(LIBMLX_DIR) fclean
	$(MAKE) -C $(LIBTOCARD_DIR) fclean

re: fclean all

valgrind: $(NAME)
	valgrind -q -s --suppressions=$(LIBMLX_DIR)/valgrind.supp --leak-check=full --show-leak-kinds=all --track-origins=yes ./$(NAME)

.PHONY: all clean fclean re

