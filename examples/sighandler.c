/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sighandler.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kiroussa <oss@xtrm.me>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/22 13:38:44 by kiroussa          #+#    #+#             */
/*   Updated: 2024/04/24 13:30:54 by kiroussa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#define _GNU_SOURCE
#include <string.h>
#include <execinfo.h>
#include <stdlib.h>
#include <signal.h>
#include <stdio.h>

static void	sighandler(int signal)
{
	char	**strings;
	void	*buffer[10];
	size_t	size;
	size_t	i;

	printf("\nCaught signal SIG%s: %s\n", sigabbrev_np(signal),
		sigdescr_np(signal));
	size = backtrace(buffer, 10);
	strings = backtrace_symbols(buffer, size);
	if (strings)
	{
		i = 0;
		while (i < size)
			printf("%s\n", strings[i++]);
		free(strings);
	}
	printf("\n");
	exit(1);
}

__attribute__((constructor))
void	sighandler_init(void)
{
	signal(SIGSEGV, sighandler);
	signal(SIGABRT, sighandler);
	signal(SIGBUS, sighandler);
	signal(SIGILL, sighandler);
	signal(SIGFPE, sighandler);
}
